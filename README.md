Installation:

* create venv by `python3 -m venv venv` in the root directory (you can change the path to venv directory by changing the second argument)
* activate venv by `source venv/bin/activate` (don't forget to change `venv` to path you specified on step 1)
* run it by `python dildosender.py -n 99` where `99` is the number of dicks you want to gift your friend!
* fucking laugh

Limitations:

* Sender is under constacrion, so it supports few shops and only one item now, but you can add the functional freely; or wait for updates.

For contributors:

* Please, don't change the main file (`dildosender.py`) itself if you are just adding new site class only. It can help in merging multiple site classes PRs.

Licensed under WTFPL (see copying for details)

[![WTFPL Logo](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png)](http://www.wtfpl.net/)