class BaseSender:
	def __init__(self, phone):
		self.phone = phone
		self.user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0'

	def save_answer(self, answer, class_name):
		with open('%s.html' % (class_name,), 'w') as m:
			m.write(answer)